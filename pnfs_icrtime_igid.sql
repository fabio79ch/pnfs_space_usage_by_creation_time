with 
 phedex       as ( select to_char(icrtime, 'yyyy-mm') as period, trunc(sum(isize)/1000/1000/1000) as       phedex_GB from t_inodes where iuid=2980 group by to_char(icrtime, 'yyyy-mm') ),

 ethz_bphys   as ( select to_char(icrtime, 'yyyy-mm') as period, trunc(sum(isize)/1000/1000/1000) as   ethz_bphys_GB from t_inodes where igid=530 group by to_char(icrtime, 'yyyy-mm') ),
 ethz_ecal    as ( select to_char(icrtime, 'yyyy-mm') as period, trunc(sum(isize)/1000/1000/1000) as    ethz_ecal_GB from t_inodes where igid=529 group by to_char(icrtime, 'yyyy-mm') ),
 ethz_ewk     as ( select to_char(icrtime, 'yyyy-mm') as period, trunc(sum(isize)/1000/1000/1000) as     ethz_ewk_GB from t_inodes where igid=531 group by to_char(icrtime, 'yyyy-mm') ),
 ethz_higgs   as ( select to_char(icrtime, 'yyyy-mm') as period, trunc(sum(isize)/1000/1000/1000) as   ethz_higgs_GB from t_inodes where igid=532 group by to_char(icrtime, 'yyyy-mm') ),
 ethz_susy    as ( select to_char(icrtime, 'yyyy-mm') as period, trunc(sum(isize)/1000/1000/1000) as    ethz_susy_GB from t_inodes where igid=533 group by to_char(icrtime, 'yyyy-mm') ),

 psi_bphys    as ( select to_char(icrtime, 'yyyy-mm') as period, trunc(sum(isize)/1000/1000/1000) as    psi_bphys_GB from t_inodes where igid=534 group by to_char(icrtime, 'yyyy-mm') ),
 psi_pixel    as ( select to_char(icrtime, 'yyyy-mm') as period, trunc(sum(isize)/1000/1000/1000) as    psi_pixel_GB from t_inodes where igid=535 group by to_char(icrtime, 'yyyy-mm') ),

 uniz_bphys   as ( select to_char(icrtime, 'yyyy-mm') as period, trunc(sum(isize)/1000/1000/1000) as   uniz_bphys_GB from t_inodes where igid=536 group by to_char(icrtime, 'yyyy-mm') ),
 uniz_higgs   as ( select to_char(icrtime, 'yyyy-mm') as period, trunc(sum(isize)/1000/1000/1000) as   uniz_higgs_GB from t_inodes where igid=537 group by to_char(icrtime, 'yyyy-mm') ),
 uniz_pixel   as ( select to_char(icrtime, 'yyyy-mm') as period, trunc(sum(isize)/1000/1000/1000) as   uniz_pixel_GB from t_inodes where igid=538 group by to_char(icrtime, 'yyyy-mm') ),

 withoutgroup as ( select to_char(icrtime, 'yyyy-mm') as period, trunc(sum(isize)/1000/1000/1000) as withoutgroup_GB from t_inodes where iuid<>2980 and igid=500 group by to_char(icrtime, 'yyyy-mm') )
 
 select * from
  
   phedex

    natural full outer join ethz_bphys  
    natural full outer join ethz_ecal 
    natural full outer join ethz_ewk 
    natural full outer join ethz_higgs
    natural full outer join ethz_susy

    natural full outer join psi_bphys
    natural full outer join psi_pixel

    natural full outer join uniz_bphys
    natural full outer join uniz_higgs
    natural full outer join uniz_pixel    

    natural full outer join withoutgroup

order by period asc ; 
